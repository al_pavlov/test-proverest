import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal";
import ModalContent from "../../components/ModalContent";
import Table from "../../components/Table";
import { TTableData } from "../../models/tableData";

const MainPage = () => {
  const [data, setData] = useState<TTableData[]>(JSON.parse(window.localStorage.getItem('data')) || []);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [editableData, setEditableData] = useState<TTableData | null>(null);

  const addHandler = (data: TTableData) => {
    setData((prev) => [...prev, data]);
    setModalIsOpen(false);
  };

  const editHandler = (data: TTableData) => {
    setData((prev) =>
      prev.reduce((acc, item) => {
        if (item.id === data.id) {
          acc.push(data);
        } else {
          acc.push(item);
        }
        return acc;
      }, [])
    );
    setEditableData(null);
    setModalIsOpen(false);
  };

  useEffect(() => {
    window.localStorage.setItem('data', JSON.stringify(data));
  }, [data]);

  return (
    <>
      <button type="button" onClick={() => {
        setModalIsOpen(true);
        setEditableData(null);
      }}>
        Add record
      </button>
      <Table
        items={data}
        deleted={(data) => {
          setData((prev) => prev.filter((i) => i.id !== data.id));
          setEditableData(null);
        }}
        edited={(data) => {
          setEditableData(data);
          setModalIsOpen(true);
        }}
      />
      {modalIsOpen && (
        <Modal closed={() => setModalIsOpen(false)}>
          <ModalContent
            added={addHandler}
            edited={editHandler}
            closed={() => setModalIsOpen(false)}
            editableData={editableData}
          />
        </Modal>
      )}
    </>
  );
};

export default MainPage;
