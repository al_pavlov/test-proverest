export type TTableData = {
  name: string;
  email: string;
  phone: string;
  id: number;
};
