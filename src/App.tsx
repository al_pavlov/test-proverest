import React from "react";

import MainPage from "./pages/Main";

import "./App.scss";

const App = () => <MainPage />;

export default App;
