import React from "react";
import { TTableData } from "../../models/tableData";

import "./index.scss";

type Props = {
  items: TTableData[];
  edited: (data: TTableData) => void;
  deleted: (data: TTableData) => void;
};

const Table = (props: Props) => {
  const { items, edited, deleted } = props;
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item) => (
          <tr key={item.id}>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{item.phone}</td>
            <td>
              <span className="action-button action-button_edit" onClick={() => edited(item)}/>
              <span className="action-button action-button_delete" onClick={() => deleted(item)} />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
