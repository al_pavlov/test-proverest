import React, { useState } from "react";
import { TTableData } from "../../models/tableData";

import "./index.scss";

const TAB_MAPPER = ["Name", "Email", "Phone"];

type Props = {
  added: (data: TTableData) => void;
  edited: (data: TTableData) => void;
  closed: () => void;
  editableData: TTableData | null;
};

const ModalContent = (props: Props) => {
  const { editableData, added, edited, closed } = props;
  const [data, setData] = useState<TTableData>(
    editableData || {
      name: "",
      email: "",
      phone: "",
      id: Date.now(),
    }
  );
  const [activeTab, setActiveTab] = useState(0);

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    switch (activeTab) {
      case 0:
        setData((prev) => ({ ...prev, name: value }));
        break;

      case 1:
        setData((prev) => ({ ...prev, email: value }));
        break;

      case 2:
        setData((prev) => ({ ...prev, phone: value }));
        break;

      default:
        break;
    }
  };

  const getInputValue = () => {
    if (activeTab === 0) {
      return data.name;
    } else if (activeTab === 1) {
      return data.email;
    } else {
      return data.phone;
    }
  };

  const updateHandler = () => {
    if (editableData) {
      edited(data);
    } else {
      added(data);
    }
  };

  return (
    <div className="modal-content">
      <div className="modal-content__header">
        {TAB_MAPPER.map((tab, index) => (
          <div
            key={tab}
            className={`modal-content__tab ${
              index === activeTab ? "_active" : ""
            }`}
            onClick={() => setActiveTab(index)}
          >
            {tab}
          </div>
        ))}
      </div>
      <div className="modal-content__body">
        <input type="text" value={getInputValue()} onChange={changeHandler} />
        <button onClick={updateHandler}>{editableData ? "Edit" : "Add"}</button>
        <button onClick={closed}>Cancel</button>
      </div>
    </div>
  );
};

export default ModalContent;
