import React, { useEffect } from "react";

import "./index.scss";

const BODY_CLASS = "modal__overflow";

type Props = {
  closed: () => void;
  children: React.ReactElement;
};

const Modal = (props: Props) => {
  const { closed, children } = props;

  useEffect(() => {
    document.body.classList.add(BODY_CLASS);
    return () => document.body.classList.remove(BODY_CLASS);
  }, []);

  return (
    <div className="modal" onClick={closed}>
      <div className="modal_content" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

export default Modal;
